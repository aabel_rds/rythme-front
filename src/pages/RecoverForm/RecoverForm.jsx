import React, { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { recoverPass, selectError } from '../../reducers/userSlice';
import GoBack from '../../components/GoBack';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faInfo } from '@fortawesome/free-solid-svg-icons'
import logo from '../../RithmeOk.png'

import './RecoverForm.scss';


const RecoverForm = (props) => {
   const dispatch = useDispatch();
   const [state,setState] = useState({});
   const error = useSelector(selectError);

   const whenRecover = () => {

      props.history.push('/login');
      setState({});
   }
   
   const handleChangeInput = (ev) => {
      const { name, value } = ev.target;
      setState({ ...state, [name]: value });
   };

   const handleSubmitForm = async ev => {

    ev.preventDefault();
    dispatch(recoverPass({
      form: state,
      c_back: whenRecover,
    }
    ));
  };

   return(
      <form className="container_recover" onSubmit={handleSubmitForm}>
      <div id="div1">
        <GoBack/>
      </div>
       <h1 id="title" className="container__title"><img src={logo} width="320px" height="160px" alt="" /></h1>
        <label id="label1" htmlFor="email">
          <input
            type="text"
            name="email"
            value={state.email}
            onChange={handleChangeInput}
            placeholder="Correo electrónico"
          />
            <FontAwesomeIcon icon={faInfo}/>
        </label>
        <div className="container_recover__button">
          <button id="button" type="submit">Enviar</button>
        </div>
        <div id="div2">
          {error && 
            <p id="error"> {error} </p>
          }
        </div>
      </form>
   )

}

export default RecoverForm;