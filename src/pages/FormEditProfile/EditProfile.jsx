import React, { useEffect } from 'react';
import { useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { selectUser, userEdit } from '../../reducers/userSlice';



import './EditProfile.scss';

const INITIAL_STATE = {
    email: "",
    username: "",
    picture: "",
    _id: ""
};
const EditProfile = (props) => {
    const [state, setState] = useState(INITIAL_STATE);
    const dispatch = useDispatch();
    const user = useSelector(selectUser);

    useEffect(() => {
    }, [user])

    const { username, email, _id } = user.user;

    const whenEdit = () => {
        props.history.push('/settings/profile');
        setState(INITIAL_STATE);

    }

    const handleSubmitForm = async ev => {

        ev.preventDefault();

        try {
            const form = new FormData();
            form.append("userName", state.username);
            form.append("email", state.email);
            form.append("picture", state.picture);
            form.append('_id', _id);

            dispatch(userEdit({
                form: form,
                c_back: whenEdit,
            }
            ));
        } catch (error) {
            console.log(error);
        }


    };

    const handleChangeInput = (ev) => {
        const { name, value } = ev.target;

        if (name === "picture") {
            let reader = new FileReader();
            let file = ev.target.files[0];

            reader.onloadend = () => {
                setState({ ...state, picture: file });
            };
            reader.readAsDataURL(file);

        } else {

            setState({ ...state, [name]: value });
        }
    };

    const cancelEditing = () => props.history.goBack();

    return (
        <div className="editprofile">
            <form onSubmit={handleSubmitForm}
                encType="multipart/form-data"
            >
                <div className="editprofile--labels">
                    <label htmlFor="username" className="editprofile--label">
                        <p>Nombre:</p>
                        <input type="text"
                            value={state.username}
                            name="username"
                            onChange={handleChangeInput}
                            className="editprofile--label__input"
                            placeholder={username}
                        />
                    </label>
                    <label htmlFor="email" className="editprofile--label">
                        <p>Email:</p>
                        <input type="text"
                            value={state.email}
                            name="email"
                            onChange={handleChangeInput}
                            className="editprofile--label__input"
                            placeholder={email}
                        />
                    </label>
                    <label htmlFor="file" className="editprofile--label">
                        <input type="file"
                            value={state.image}
                            name="picture"
                            onChange={handleChangeInput}
                            className="editprofile--label__input__file"
                            accept="image/png, image/jpg, image/jpeg"
                        />
                    </label>
                </div>
                <div className="addcomment">
                    <button className="addcomment__buttom" type="submit">Actualizar</button>
                    <button className="addcomment__buttom" onClick={cancelEditing}  >Cancelar</button>
                </div>
            </form>
        </div>
    );
}

export default EditProfile;


