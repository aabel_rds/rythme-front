import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Link, Redirect } from "react-router-dom";
import GoBack from "../../components/GoBack/";
import { loginAsync, selectError, selectHasUser } from '../../reducers/userSlice';
import logo from '../../RithmeOk.png'

import './LoginForm.scss';

// 8 characters, upper case , lower case and númbers;
const INITIAL_STATE = {
  email: "",
  password: "",
};

const LoginForm = (props) => {
  const dispatch = useDispatch();
  const [state, setState] = useState(INITIAL_STATE);
  const error = useSelector(selectError);
  const hasUser = useSelector(selectHasUser);

  const whenLogin = () => {
    props.history.push('/home');
    setState(INITIAL_STATE);
  }

  const handleSubmitForm = async ev => {

    ev.preventDefault();

    dispatch(loginAsync({
      form: state,
      c_back: whenLogin,
    }
    ));
  };

  const handleChangeInput = (ev) => {
    const { name, value } = ev.target;
    setState({ ...state, [name]: value });
  };

  if (hasUser === true) {
    return <Redirect to="/home" />;
  } else {

    return (
      <form className="container" onSubmit={handleSubmitForm}>
        <div id="div1">
          <GoBack />
        </div>
        <h1 id="title" className="container__title"><img src={logo} width="320px" height="160px" alt=""/></h1>
        <label id="label1" htmlFor="email">
          <input
            type="text"
            name="email"
            value={state.email}
            onChange={handleChangeInput}
            placeholder="Correo electrónico"
          />
        </label>
        <label id="label2" htmlFor="password">
          <input
            type="password"
            name="password"
            value={state.password}
            onChange={handleChangeInput}
            placeholder="Contraseña"
          />
        </label>
        <div className="container_button">
          <button id="button" type="submit">Login</button>
        </div>
        <Link to="/login/recover" id="div3" >
          ¿ Has olvidado la contraseña ?
      </Link>
        <div id="div2">
          {error &&
            <p id="error"> {error} </p>
          }
        </div>
      </form>
    );
  }
};

export default LoginForm;
