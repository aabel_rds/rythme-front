import React, { useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { registerAsync } from '../../reducers/userSlice';
import {selectError} from '../../reducers/userSlice';
import GoBack from '../../components/GoBack/';
import logo from '../../RithmeOk.png'

import './RegisterForm.scss'

const INITIAL_STATE = {
  username: "",
  email: "",
  password: "",
  error: "",
};

const RegisterForm = (props) => {

  const dispatch = useDispatch();
  const [state, setState] = useState(INITIAL_STATE);
  const error = useSelector(selectError);

  const whenRegister = () =>{
    props.history.push('/');
    setState(INITIAL_STATE);
  }

  const handleSubmitForm = async (ev) => {
    
    ev.preventDefault();
    delete state.error;

    dispatch(registerAsync({
      form: state,
      c_back: whenRegister,
    }
    ));
  };

  const handleChangeInput = (ev) => {

    const { name, value } = ev.target;
    setState({ ...state, [name]: value });
  };

  return (
    
    <form className="Register_container" onSubmit={handleSubmitForm}>
    <div id="div1">
      <GoBack />
    </div>
      <h1 id="title" className="Register_container__title"><img src={logo} width="320px" height="160px" alt="" /></h1>
      <label id="label1" htmlFor="username">
        <input
          type="text"
          name="username"
          value={state.username}
          onChange={handleChangeInput}
          placeholder="Usuario"
        />
      </label>
      <label id="label2" htmlFor="email">
        <input
          type="text"
          name="email"
          value={state.email}
          onChange={handleChangeInput}
          placeholder="Correo electrónico"
        />
      </label>
      <label id="label3" htmlFor="password">
        <input
          type="password"
          name="password"
          value={state.password}
          onChange={handleChangeInput}
          placeholder="Contraseña"
        />
      </label>
      <div className="Register_container_button">
        <button id="button" type="submit">Registo de cuenta</button>
      </div>
      <div id="div2">
          {error && 
            <p id="error"> {error} </p>
          }
        </div>
    </form>
  );
};

export default RegisterForm;
