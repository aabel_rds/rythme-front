import React, { useState, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { withRouter } from 'react-router-dom';
import { registerTicket } from "../../reducers/ticketSlice";
import { selectUser } from "../../reducers/userSlice";

import "./Payment.scss";

const INITIAL_STATE = {
    name: "",
    email: "",
    postelcode: "",
    tickets: null,
    singer: "",
    hall: "",
    date: "",
    hour: "",
    price: "",
};

const COMISSION_LOG = 0.90;


export const Payment = (props) => {
    const dispatch = useDispatch();
    const [ticket, setTicket] = useState({})
    const [totalValue, setValue] = useState(0)
    const [buyTicket, setBuyTicket] = useState(INITIAL_STATE)
    // const error = useSelector(selectError);
    const user = useSelector(selectUser);


    const newLocal = [props.match.params.id];
    useEffect(() => {
        fecthTicket()
    }, newLocal)

    const whenBuyTicket = () => {

        props.history.push("/loadingbuy");
        setBuyTicket(INITIAL_STATE);
    }

    const fecthTicket = async () => {
        const response = await fetch(`https://rithme.herokuapp.com/concert/${props.match.params.id}`)
        const buyticket = await response.json()
        if (!buyticket.concert) props.history.push("/tickets")
        setTicket(buyticket)

        const { artist, concertHall, date, hour, price } = buyticket.concert

        const singer = artist[0];
        const hall = concertHall[0]

        setBuyTicket({ ...buyTicket, singer, hall, date, hour, price });

    }

    const handleSelecTicket = (ev) => {
        const sum = ((ev.target.value * ticket.concert.price) + COMISSION_LOG)


        const tickets = {
            totaltickets: ev.target.value,
            totalvalue: sum,
            userId: user.user._id
        }


        setValue(sum)
        setBuyTicket({ ...buyTicket, tickets });
    }

    const handleBuyTicket = async ev => {
        ev.preventDefault();


        dispatch(registerTicket({
            form: buyTicket,
            c_back: whenBuyTicket,
        }))

    }

    const handleInputTicket = (ev) => {
        const { name, value } = ev.target;
        setBuyTicket({ ...buyTicket, [name]: value, });
    }

    return (
        <div className="box">
            <div className="box--navbar">
                <p className="box--navbar__text">Compra tus entradas</p>
            </div>
            <div className="box--ticket">
                <p className="box--ticket__text">Entrada General: {ticket.concert && ticket.concert.price} €</p>
                <select onChange={handleSelecTicket} className="box--ticket__number" name="" id="">
                    <option value={0}> </option>
                    <option value={1}>1 Entrada</option>
                    <option value={2}>2 Entradas</option>
                    <option value={3}>3 Entradas</option>
                    <option value={4}>4 Entradas</option>
                </select>
                <span className="box--ticket__log" >Gastos de gestion: 0.90€</span>
                <div className="line"> </div>
                <p className="box--ticket__price" id="spTotal" >Total: {totalValue} €</p>
            </div>
            <form onSubmit={handleBuyTicket} className="box--dates">
                <h2 className="box--dates__title">Datos del Comprador</h2>
                <input
                    onChange={handleInputTicket}
                    name="name"
                    type="text"
                    className="box--dates__date"
                    placeholder=" Nombre y Apellidos"
                    required />
                <input
                    onChange={handleInputTicket}
                    name="email"
                    type="email"
                    className="box--dates__date"
                    placeholder=" Email"
                    required />
                <input onChange={handleInputTicket}
                    name="postelcode"
                    type="number"
                    className="box--dates__date"
                    placeholder=" Codigo Postal"
                    required />
                <button type="submit" className="pay">Pagar</button>
            </form>
            {/* {error &&
                <p id="error"> {error} </p>
            } */}
        </div>
    )
}

export default withRouter(Payment);
