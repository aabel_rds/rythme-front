import React, { useState, useEffect } from "react";
import { useSelector } from "react-redux";
import './Home.scss';
import { selectUser } from '../../reducers/userSlice';
import { MenuButtons } from "../../components/MenuButtons/MenuButtons";
import SliderItem from "../../components/SliderItem";


const Carousel = (props) => {

  const [artistList, setArtistList] = useState([]);
  const [concertHallList, setConcertHallList] = useState([]);
  const [styleList, setStyleList] = useState([]);
  const [concertList, setConcertList] = useState([]);
  const User = useSelector(selectUser)

  useEffect(() => {
    apiArtist()
    apiConcertHall()
    apiStyle()
    apiConcert()
  }, [])

  const apiArtist = async () => {
    const response = await fetch("https://rithme.herokuapp.com/artist")
    const artist = await response.json()
    setArtistList(artist)
  }
  const apiConcertHall = async () => {
    const response = await fetch("https://rithme.herokuapp.com/concertHall")
    const concertHall = await response.json()
    setConcertHallList(concertHall)
  }
  const apiStyle = async () => {
    const response = await fetch("https://rithme.herokuapp.com/style")
    const style = await response.json()
    setStyleList(style)
  }
  const apiConcert = async () => {
    const response = await fetch("https://rithme.herokuapp.com/concert")
    const concert = await response.json()
    setConcertList(concert, "API CONCERT")
  }
  return (
    <div className="home">
      <MenuButtons />
      <div className="home--user">
        <span className="home--user__name"> ¡Hola {User.user.username}! </span>
        <p className="home--user__text">¿Qué concierto te apetece?</p>
      </div>
      <div className="sections">
        <h2 className="home--sections">ARTISTAS</h2>
        <div className="slider">
          <div className="slider__container">
            {
              artistList.map(artist => (
                <SliderItem key={JSON.stringify(artist)} artist={artist} concertList={concertList} />
              ))
            }
          </div>
        </div>
      </div>
      <div className="sections">
        <h2 className="home--sections">ESTILOS</h2>
        <div className="slider">
          <div className="slider__container">
            {
              styleList.map(style => (
                <div className="slider__img" key={JSON.stringify(style)}>
                  <img className="image" src={style.picture} alt={style.name}></img>
                </div>
              ))
            }
          </div>
        </div>
      </div>
      <div className="sections">
        <h2 className="home--sections">SALAS</h2>
        <div className="slider">
          <div className="slider__container">
            {
              concertHallList.map(concerthall => (
                <div className="slider__img" key={JSON.stringify(concerthall)}>
                  <img className="image" src={concerthall.image} alt={concerthall.name}></img>
                </div>
              ))
            }
          </div>
        </div>
      </div>
    </div>
  )
}

export default Carousel;


