import React, { useState, useEffect } from "react";
import { Link } from 'react-router-dom';
import './Concerts.scss';
import MenuButtons from "../../components/MenuButtons";

const Concerts = (props) => {

    const [concertList, setConcertList] = useState([]);
    useEffect(() => {
        apiConcert()
    }, [])
    const apiConcert = async () => {
        const response = await fetch("https://rithme.herokuapp.com/concert")
        const concert = await response.json()
        setConcertList(concert)
    }
    return (
        <div className="concert-list">
            <MenuButtons />
            <ul className="concerts__ul">
                {
                    concertList.concerts && concertList.concerts.map(concert => (
                        <li className="concerts__ul__li" key={JSON.stringify(concert._id)}>
                            <h2 className="concerts__ul__li__date">{concert.date}</h2>
                            <div className="concerts">
                                <div className="concerts__img">
                                    {
                                        concert.artist && concert.artist.map(artist => (
                                            <div key={JSON.stringify(artist._id)}>
                                                <img src={artist.picture} alt={artist.name}  width="130px" height="120px" ></img>
                                            </div>
                                        ))
                                    }
                                </div>
                                <div className="concerts__room">
                                    {
                                        concert.concertHall && concert.concertHall.map(concertHall => (
                                            <p key={JSON.stringify(concertHall._id)}>{concertHall.name}</p>
                                        ))
                                    }
                                </div>
                                <div className="concerts__artist">
                                    {
                                        concert.artist && concert.artist.map(artist => (
                                            <h3 key={JSON.stringify(artist._id)}>{artist.name}</h3>
                                        ))
                                    }
                                </div>
                                <div className="concerts__gender">
                                    {
                                        concert.artist && concert.artist.map(artist => (
                                            <p key={JSON.stringify(`${artist._id}gender`)}>{artist.gender}</p>
                                        ))
                                    }
                                </div>
                                <Link to={`/ticket/${concert._id}`}>
                                    <button className="concerts__buy">Comprar</button>
                                </Link>
                                <div className="concerts__price">
                                    <h5>€ {concert.price}</h5>
                                </div>
                            </div>
                            <hr></hr>
                        </li>
                    ))
                }
            </ul>
        </div>

    )
}

export default Concerts;