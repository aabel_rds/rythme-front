import React, { useState, useEffect } from "react";
import { Link } from "react-router-dom";

import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faMapMarker, faMicrophone, faMusic } from '@fortawesome/free-solid-svg-icons';

import "./ConcertDetail.scss";


const ConcertDetail = ({ match }) => {

  const [detailConcert, setDetailConcert] = useState({});

  useEffect(() => {
    apiConcert();
  }, [match.params.id]);

  const apiConcert = async () => {
    const response = await fetch(`https://rithme.herokuapp.com/concert/${match.params.id}`)
    const detail = await response.json();
    setDetailConcert(detail)

  }

  return (
    <div className="concert2">
      <div className="concert--card">
        <img className="concert--card__image" src={detailConcert.concert && detailConcert.concert.artist[0].picture} alt="concer hall imagen" />
        <div className="concert--date">
          <h2 className="concert--card__name">{detailConcert.concert && detailConcert.concert.artist[0].name}</h2>
          <p className="concert--card__date">{detailConcert.concert && detailConcert.concert.date}</p>
          <p className="concert--card__hour">{detailConcert.concert && detailConcert.concert.hour}</p>
          <p className="concert--card__price">{detailConcert.concert && detailConcert.concert.price} €</p>
        </div>
      </div>
      <div className="concert--link">
        <Link to={`/ticket/${match.params.id}/buy-ticket`}>
          <button className="concert--link__button">Comprar</button></Link>
        <button className="concert--link__buttonfav">Compartir</button>
      </div>
      <div className="concert--description">
        <div className="concert--descriptions">
          <FontAwesomeIcon icon={faMapMarker} className="concert--descriptions__icons" />
          <p>{detailConcert.concert && detailConcert.concert.concertHall[0].name} - {detailConcert.concert && detailConcert.concert.concertHall[0].city}</p>
        </div>
        <div className="concert--descriptions">
          <FontAwesomeIcon icon={faMicrophone} className="concert--descriptions__icons" />
          <p>{detailConcert.concert && detailConcert.concert.artist[0].name}</p>
        </div>
        <div className="concert--descriptions">
          <FontAwesomeIcon icon={faMusic} className="concert--descriptions__icons" />
          <p>{detailConcert.concert && detailConcert.concert.artist[0].gender}</p>
        </div>
      </div>
      <div className="concert--detail">
        <p>{detailConcert.concert && detailConcert.concert.artist[0].description}</p>
      </div>
    </div>
  )
}

export default ConcertDetail;