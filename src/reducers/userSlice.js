import { createSlice } from '@reduxjs/toolkit';

const registerUrl = "https://rithme.herokuapp.com/auth/register";
const loginUrl = "https://rithme.herokuapp.com/auth/login";
const checkSessionUrl = "https://rithme.herokuapp.com/auth/check-session";
const logoutUrl = "https://rithme.herokuapp.com/auth/logout";
const recoverUrl = "https://rithme.herokuapp.com/auth/recover-pass";
const editUrl = "https://rithme.herokuapp.com/auth/edit";

export const userSlice = createSlice({
    name: 'user',
    initialState: {
        hasUser: false,
        user: null,
        error: null,
    },
    reducers: {

        setUser: (state, action) => {

            state.user = action.payload;
        },
        setError: (state, action) => {

            state.error = action.payload;
        },
        hasUser: (state, action) => {
            state.hasUser = action.payload;
        }
    },
});

const { setUser, setError, hasUser } = userSlice.actions;

export const checkUserSession = () => async dispatch => {
    const request = await fetch(checkSessionUrl, {
        method: 'GET',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        credentials: 'include',
    });
    const response = await request.json();
    if (request.ok) {

        dispatch(setUser(response));
        dispatch(hasUser(true));
    }
};

export const registerAsync = ({ form, c_back }) => async dispatch => {
    const request = await fetch(registerUrl, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            'Access-Control-Allow-Credentials': true,
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        body: JSON.stringify(form),
    })

    const response = await request.json();
    console.log(response.message);

    if (!request.ok) {
        dispatch(setError(response.message));
    } else {
        dispatch(setUser(response));
        dispatch(hasUser(true));
        dispatch(setError(null));
        c_back();
    }
};

export const loginAsync = ({ form, c_back }) => async dispatch => {
    console.log(form);
    const request = await fetch(loginUrl, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "http://localhost:*",
            "Access-Control-Allow-Credentials": true,
        },
        credentials: 'include',
        body: JSON.stringify(form),
    })

    const response = await request.json();
    console.log(response.message);
    if (!request.ok) {
        dispatch(setError(response.message));
    } else {
        console.log(hasUser);
        dispatch(setUser(response));
        dispatch(hasUser(true));
        dispatch(setError(null));
        c_back();

    }
};

export const logoutUser = () => async dispatch => {
    const request = await fetch(logoutUrl, {
        method: 'POST',
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "*"
        },
        credentials: 'include',
    });

    await request.json();

    dispatch(setUser(null));
    dispatch(hasUser(false));
}

export const recoverPass = ({ form, c_back }) => async dispatch => {
    console.log(form, 'recoverPass formRequest');
    const request = await fetch(recoverUrl, {
        method: "POST",
        headers: {
            "Accept": "application/json",
            "Content-Type": "application/json",
            "Access-Control-Allow-Origin": "http://localhost:*",
            "Access-Control-Allow-Credentials": true,
        },
        credentials: 'include',
        body: JSON.stringify(form)
    });
    const response = await request.json();
    console.log(response);
    if (!request.ok) {
        dispatch(setError(response.message));
    } else {
        dispatch(setError(response.message));
        console.log(response.message, 'nodemailer');
        dispatch(hasUser(false));
        c_back();
    };
}

export const userEdit = ({ form, c_back }) => async dispatch => {

    console.log(form, 'recoverPass formRequest');
    const request = await fetch(editUrl, {
        method: "POST",
        headers: {
            "Accept": "*",
            "Access-Control-Allow-Origin": "*",
        },
        credentials: 'include',
        body: form
    });

    const response = await request.json();
    console.log(response);
    if (!request.ok) {
        dispatch(setError(response.message));
    } else {
        dispatch(setError(response.message));
        console.log(response.message, 'nodemailer');
        c_back();
    };
}

export const selectHasUser = state => state.user.hasUser;
export const selectUser = state => state.user;
export const selectError = state => state.user.error;

export default userSlice.reducer;