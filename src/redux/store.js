import { configureStore } from '@reduxjs/toolkit';
import userReducer from '../reducers/userSlice';
import ticketReducer from '../reducers/ticketSlice'
export default configureStore({
    reducer: {
        user: userReducer,
        ticket: ticketReducer,
    }
});