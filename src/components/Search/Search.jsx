import React, { useState, useEffect } from 'react';
import { Link } from 'react-router-dom';

import "./Search.scss"

const Search = () => {

    const [concertsList, setConcertsList] = useState([])
    const [results, setResults] = useState([])
    const [active, setActive] = useState(false)

    useEffect(() => {
        apiConcert()
    }, [])

    const apiConcert = async () => {
        const response = await fetch("https://rithme.herokuapp.com/concert")
        const concert = await response.json()
        setConcertsList(concert.concerts, "API CONCERT");
        setResults(concert.concerts, "API CONCERT")
    }


    const onchangeInput = (ev) => {
        const { value } = ev.target
        const filter = concertsList.filter((concert) => {
            const artistName = concert.artist[0].name.toLowerCase()
            return artistName.includes(value.toLowerCase());
        })
        setResults(filter);
        setActive(true)
        if (value === ("")) { return setActive(false) }

    }

    if (!active) return (
        <div className="searching">
            <h2 className="searching__txt">Busca a tu artista preferido</h2>
            <input
                className="searching__input"
                type="text"
                name="search"
                onChange={onchangeInput}
            />
        </div>
    )
    return (
        <div className="searchingactive">
            <h2 className="searching__txt">Busca a tu artista preferido</h2>
            <input
                className="searching__input"
                type="text"
                name="search"
                onChange={onchangeInput} />
            <div >
                {
                    results && results.map(concert => (
                        <li className="concerts__ul__li" key={JSON.stringify(concert)}>
                            <h2 className="concerts__ul__li__date">{concert.date}</h2>
                            <div className="concerts">

                                <div className="concerts__img">
                                    {
                                        concert.artist && concert.artist.map(artist => (
                                            <img src={artist.picture} alt={artist.name} width="130px" height="120px"></img>
                                        ))
                                    }
                                </div>
                                <div className="concerts__room">
                                    {
                                        concert.concertHall && concert.concertHall.map(concertHall => (
                                            <p>{concertHall.name}</p>
                                        ))
                                    }
                                </div>
                                <div className="concerts__artist">
                                    {
                                        concert.artist && concert.artist.map(artist => (
                                            <h3>{artist.name}</h3>
                                        ))
                                    }
                                </div>
                                <div className="concerts__gender">
                                    {
                                        concert.artist && concert.artist.map(artist => (
                                            <p>{artist.gender}</p>
                                        ))
                                    }
                                </div>
                                <Link to={`/ticket/${concert._id}`}>
                                    <button className="concerts__buy">Comprar</button>
                                </Link>
                                <div className="concerts__price">
                                    <h5>€ {concert.price}</h5>
                                </div>
                            </div>
                        </li>
                    ))
                }
            </div>
        </div>
    )
}

export default Search
