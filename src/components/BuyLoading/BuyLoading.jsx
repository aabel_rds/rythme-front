import React from 'react'
import { useSelector } from "react-redux";
import { selectUser } from '../../reducers/userSlice';
import { withRouter, Link } from 'react-router-dom';

import './BuyLoading.scss';

const BuyLoading = (props) => {

    const User = useSelector(selectUser)
    return (
        <div>
            <div className="confirming">
                <div className="confirming--user">
                    <h2 className="confirming--user__name"> ¡Genial {User.user.username}! </h2>
                    <div className="confirming--text">
                        <h3 className="confirming--text__rem">Que disfrutes del concierto.</h3>
                        <p className="confirming--text__rem">Rhythme te ha enviado un correo con tu entrada y comprobante.</p>
                        <p className="confirming--text__rem">Recuerda que siempre podras acceder a tu entrada desde la App RYTHME</p>
                    </div>
                    <button className="btn--ticket">
                        <Link to="/youticket">
                            <p> Descargar la entrada</p>
                        </Link>
                    </button>
                    <button className="btn--home">
                        <Link to="/">
                            <p className="btn--home__txt"> Mas tarde</p>
                        </Link>
                    </button>
                </div>
            </div>
        </div>
    )
}

export default withRouter(BuyLoading);
