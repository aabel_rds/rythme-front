import React from 'react'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'
import { faCopyright } from '@fortawesome/free-solid-svg-icons'
import { withRouter } from "react-router-dom";
import './Help.scss'

const Help = () => {
    return (
        <div className="help">
            <div className="help--text">
                <h1 className="help--text__title">¿Necesitas Ayuda?</h1>
                <p className="help--text__txt">Hola Rythmaniaco, ¿necesitas ayuda para comprar una entrada?, ¿no sabes logearte?, ¿la aplicación te ha metido Malwares Rusos?</p>
                <p className="help--text__txt">No te preocupes contacta a nuestro servicio tecnico experto en solucionar todos tus problamas o simplente quieres contratarnos para tu gran empresa</p>
            </div>
            <div className="help--contacts">
                <ul className="help--contacts__list">
                    <li className="help--contacts__list__name">Maria Martin <span className="yellow"> <a href="maria.mhzd@gmail.com"></a> maria.mhzd@gmail.com </span> </li>
                    <li className="help--contacts__list__name">Alejandro Abel <span className="yellow">  aabelrds@gmail.com </span></li>
                    <li className="help--contacts__list__name">Alvaro del Castillo <span className="yellow">  alvarcv@gmail.com </span></li>
                </ul>
            </div>
            <span className="copy">Todos los derechos reservados  <FontAwesomeIcon icon={faCopyright} /> </span>
        </div>
    )
}

export default withRouter(Help)
