import React from 'react'
import { withRouter } from "react-router-dom";
import icon from '../../icons/arrow/left/leftarrow2.png'
import "./GoBack.scss";

const GoBack = (props) => {

   return (
      <withRouter>
         <div className="goBack">
            <span className="goBack__arrow">
               <img
                  src={icon}
                  onClick={() => { props.history.goBack() }}
                  alt="GoBack icon"
                  className="icon--arrow"
               />
            </span>
         </div>
      </withRouter>
   )
}


export default withRouter(GoBack);