import React from 'react';

import './ProfileCard.scss';

const ProfileCard = (props) => {

   const {username, email, image} = props.user.user;

   return(
         <div className="card">
            <div className="card__image">
               <img src={image}alt=""/>
            </div>
            <div className ="tex-container">
               <p className ="tex-container__title" >Usuario</p>
               <p className ="tex-container__text">{ username }</p>
               <div className="divline"/>
               <p className ="tex-container__title">Email</p>
               <p className="tex-container__text">{ email }</p>
               <div className="divline"/>
            </div>
         </div>
   )
}

export default ProfileCard;