import React from 'react';
import { withRouter } from "react-router-dom";
import './SliderItem.scss'

const SliderItem = (props) => {

    const goToConcert = () => {
        const concerts = props.concertList.concerts;
        const artistConcert = concerts.find(concert => {
            return props.artist._id === concert.artist[0]._id;
        })
        props.history.push(`/ticket/${artistConcert._id}`)
    }
    return (
        <div onClick={goToConcert} className="slider__img" >
            <img className="image" src={props.artist.picture} alt={props.artist.name}></img>
        </div>
    )
}

export default withRouter(SliderItem);