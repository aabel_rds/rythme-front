import React from 'react';
import "./Streaming.scss";

const Streaming = () => {

    return (
        <div className="sincronice">
            <h3 className="title">SINCRONIZA TU MÚSICA Y PLATAFORMAS</h3>
            <div className="streaming">
                <div className="space" >
                    <div className="streaming-music">
                        <a href="https://www.youtube.com/"><img src="https://logos-marcas.com/wp-content/uploads/2020/04/YouTube-Logo.png" alt="logo youtube" className="streaming-music__image" ></img></a>
                    </div>
                    <div className="streaming-music">
                        <a href="https://play.google.com/store/apps/details?id=com.google.android.music&hl=es&gl=US"><img src="https://androidcommunity.com/wp-content/uploads/2017/04/Google-Play-Music-Logo.jpg" alt="logo google play" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music">
                        <a href="https://www.amazon.es/music/prime"><img src="https://www.gamerevolution.com/assets/uploads/2019/04/amazon-music.jpg" alt="amazon music logo" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music">
                        <a href="https://soundcloud.com/"><img src="https://imageproxy.themaven.net/https:%2F%2Fs3-us-west-2.amazonaws.com%2Fmaven-user-photos%2Fbd7e33cc-ad11-4925-844f-a2e9ff2e5a9a" alt="soundcloud logo" className="streaming-music__image" ></img></a>
                    </div>
                </div>
                <div div className="space">
                    <div className="streaming-music">
                        <a href="https://www.spotify.com/es/home/"><img src="https://logos-marcas.com/wp-content/uploads/2020/09/Spotify-Logo.png" alt="logo spotify" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music">
                        <a href="https://music.apple.com/us/browse"><img src="https://logos-marcas.com/wp-content/uploads/2020/11/Apple-Music-Logotipo-2015-presente.jpg" alt="logo apple music" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music">
                        <a href="https://www.deezer.com/es/"><img src="https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcQu6FGOAjabVMzGAGUqTpEEYX-8P8oxxfaKOA&usqp=CAU" alt="logo deezer" className="streaming-music__image"></img></a>
                    </div>
                    <div className="streaming-music">
                        <a href="https://tidal.com/"> <img src="https://logos-download.com/wp-content/uploads/2016/06/Tidal_logo.png" alt="tidal logo" className="streaming-music__image" ></img></a>
                    </div>
                </div>
            </div>
        </div>
    )
}

export default Streaming;


