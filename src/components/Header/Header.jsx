import React from 'react'
import { withRouter } from 'react-router-dom'
import  GoBack  from '../GoBack'

import "./Header.scss"

const dontShowRouter = ["/", "/login", "/register", "/login/recover", "/loadingbuy"]

const Header = (props) => {
    if (dontShowRouter.indexOf(props.location.pathname) > -1) return null;
    return (
        <div className="header">
            <div className="header__left">
                <GoBack />
            </div>
            <div className="header__center">
                {props.location.pathname.replace(/\//, '', ``).toUpperCase()}
            </div>
        </div>
    )

}

export default withRouter(Header)