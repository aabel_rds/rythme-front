import React, { useState } from 'react';
import Calendar from 'react-calendar';

import 'react-calendar/dist/Calendar.css';
import './CalendarRytme.scss'

const CalendarRytme = () => {
    const [value, onChange] = useState(new Date());
    return (
        <div className="calendar">
            <Calendar
                onChange={onChange}
                value={value}
            />
        </div>
    )
}


export default CalendarRytme;