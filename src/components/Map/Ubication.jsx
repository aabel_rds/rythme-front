import React from 'react'
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";
import MapView from "../../componentsMap/MapView";
import Home from "../../componentsMap/Home";

import './Ubication.scss'

const Ubication = () => {
    return (
        <div className="gps--map">
            <Router>
                <Switch>
                    <Route path="/map">
                        <MapView />
                    </Route>
                    <Route path="/">
                        <Home />
                    </Route>
                </Switch>
            </Router>
        </div>
    )
}

export default Ubication;