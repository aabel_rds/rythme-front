import React from "react";
import { useSelector } from "react-redux";
import { selectTicket } from "../../reducers/ticketSlice";
import { withRouter } from 'react-router-dom';
import QRCode from "qrcode.react";

import "./ConfirmingBuy.scss"

export const ConfirmingBuy = () => {

    const tickets = useSelector(selectTicket);
    const lastTicket = tickets.ticket;

    
    return (
        <div className="confirming2">
            <widget type="ticket" className="--flex-column">
                <div className="top --flex-column">
                    <div className="bandname -bold">{lastTicket.singer.name}</div>
                    <div className="tourname">{lastTicket.hall.name}</div>
                    <img src={lastTicket.singer.picture} width="200px" alt="" />
                    <div className="deetz --flex-row-j!sb">
                        <div className="event --flex-column">
                            <div className="date">{lastTicket.date}</div>
                            <div className="location -bold">{ lastTicket.hall.location }</div>
                        </div>
                        <div className="price --flex-column">
                            <div className="label">Price</div>
                            <div className="cost -bold">{ lastTicket.price } €</div>
                        </div>
                    </div>
                </div>
                <div className="rip"></div>
                <div className="bottom --flex-row-j!sb">
                    <div className="qr">
                        {["L"].map(level => (
                            <div>
                                <QRCode
                                    value={lastTicket.singer.picture}
                                    renderAs="svg"
                                    level={level}
                                    fgColor="#333"
                                    bgColor="#fff"
                                    key={`level-${level}`}
                                />

                            </div>
                        ))}
                    </div>
                    <div className="buy" >IMPRIME TICKET</div>
                </div>
            </widget>
        </div>
    )
};

export default withRouter(ConfirmingBuy);
