import React, { useState } from 'react';

import './Input.scss';



const Input = (props) => {
   const tool = props.input;
   const event = props.event;
   const INITIAL_STATE = {
      email: "",
      username: "",
      image: ""
   };
   const [state, setState] = useState(INITIAL_STATE);

   const handleChangeInput = (ev) => {
      const { name, value } = ev.target;
      setState({ ...state, [name]: value });
      event(name, value);
   };


   return (
      <label htmlFor={props.input.type}>
         <input
            type={tool}
            name={tool}
            value={state.initial}
            placeholder={tool}
            className="input"
            onChange={handleChangeInput}
         />
      </label>
   )
}

export default Input;